// Simple Calculator
// Sawyer Grambihler

#include <iostream>
#include <conio.h>

using namespace std;

struct Employee
{
	int Id;
	string FName;
	string LName;
	double PayRate;
	double Hours;
};

double calculateTotalPay(Employee employees[], int);

int main() {

	int numEmployees;
	cout << "How many employees do you have?\n";
	cin >> numEmployees;

	Employee* employees = new Employee[numEmployees];

	double totalPay;

	for (int i = 0; i < numEmployees; i++) {
		cout << "Enter the id of employee #" << i + 1 << "\n";
		cin >> employees[i].Id;
		cout << "Enter the first name of employee #" << i + 1 << "\n";
		cin >> employees[i].FName;
		cout << "Enter the last name of employee #" << i + 1 << "\n";
		cin >> employees[i].LName;
		cout << "Enter the pay rate of employee #" << i + 1 << "\n";
		cin >> employees[i].PayRate;
		cout << "Enter the hours worked by employee #" << i + 1 << "\n";
		cin >> employees[i].Hours;
	}

	for (int i = 0; i < numEmployees; i++)
	{
		cout << "Employee " << employees[i].Id << ", " << employees[i].FName << " " << employees[i].LName << ", earned $" << (employees[i].PayRate * employees[i].Hours) << "\n";
	}

	cout << "Total gross pay for all employees: $" << calculateTotalPay(employees, numEmployees);

	_getch();
	return 0;
}

double calculateTotalPay(Employee employees[], int numEmployees) {
	double output = 0;
	for (int i = 0; i < numEmployees; i++) {
		output = output + (employees[i].PayRate * employees[i].Hours);
	}

	return output;
}